function setblock(p){
function updatePrice() {
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType-"+p);
  let select = s[0];
  let textarea=Number(document.getElementById("kol-"+p).value);
  let price =Number(document.getElementById("constPrice-"+p).textContent);
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
	if(prices.prodTypes[priceIndex]!=0){
    price = (price+prices.prodTypes[priceIndex])*textarea;
  }else{
	  price*=textarea;
  };
  }
  
  // Скрываем или показываем радиокнопки.
  let radioDiv = document.getElementById("radios-"+p);
  radioDiv.style.display = (select.value == "1" || select.value =="3" ? "none" : "block");
  // Смотрим какая товарная опция выбрана.
  let MyElementName = document.getElementById("radios-"+p);
  if (MyElementName.style.display != "none"){
  let radios = document.getElementsByName("prodOptions-"+p);
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  })};

  // Скрываем или показываем чекбоксы.
  let checkDiv = document.getElementById("checkboxes-"+p);
  checkDiv.style.display = (select.value == "1" || select.value=="2"? "none" : "block");

  // Смотрим какие товарные свойства выбраны.
  let MyElement = document.getElementById("checkboxes-"+p);
  if (MyElement.style.display != "none"){
  let check=document.querySelector("#checkboxes-"+p);
  let checkboxes = check.querySelectorAll("input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price +=textarea*propPrice;
      }
    }
  })};
  
  let prodPrice = document.getElementById("prodPrice-"+p);
  prodPrice.innerHTML = price + "₽";
}

function getPrices() {
  return {
    prodTypes: [0,50,100],
    prodOptions: {
	  option1: 45,
      option2: 30,
      option3: 25,
    },
    prodProperties: {
      prop1: 46,
      prop2: 23,
    }
  };
}

window.addEventListener('DOMContentLoaded',function (event) {
  // Скрываем радиокнопки.
  let radioDiv = document.getElementById("radios-"+p);
  radioDiv.style.display = "none";
  
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType-"+p);
  let select = s[0];
  // Назначаем обработчик на изменение select.
  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
  let textarea=document.getElementById("kol-"+p);
  textarea.addEventListener("change",function(event){
	  let f=event.target;
	  console.log(f.value);
	  updatePrice();
  });
  // Назначаем обработчик радиокнопок.  
  let radios = document.getElementsByName("prodOptions-"+p);
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });
	select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
    // Назначаем обработчик радиокнопок.  
  let check=document.querySelector("#checkboxes-"+p);  
  let checkboxes = check.querySelectorAll("input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });
  updatePrice();
})
};

function setBLocks(){
	var inputs = document.getElementById("PRODUCT_list");
	var input = inputs.children;
	for(var i=0;i<input.length;i++){
		var pol=input[i].children[5].childNodes[1].childNodes[7];
		console.log(pol);
		var l=String(i);
		setblock(l);
	}
}
setBLocks();
/*Скрипт для корзины
var d = document,
    itemBox = d.querySelectorAll('.item_box'), // блок каждого товара
		cartCont = d.getElementById('cart_content'); // блок вывода данных корзины
// Функция кроссбраузерная установка обработчика событий
function addEvent(elem, type, handler){
  if(elem.addEventListener){
    elem.addEventListener(type, handler, false);
  } else {
    elem.attachEvent('on'+type, function(){ handler.call( elem ); });
  }
  return false;
}// Получаем данные из LocalStorage
function getCartData(){
	return JSON.parse(localStorage.getItem('cart'));
}
// Записываем данные в LocalStorage
function setCartData(o){
	localStorage.setItem('cart', JSON.stringify(o));
	return false;
}
// Добавляем товар в корзину
function addToCart(e){
	this.disabled = true; // блокируем кнопку на время операции с корзиной
	var cartData = getCartData() || {}, // получаем данные корзины или создаём новый объект, если данных еще нет
			parentBox = this.parentNode, // родительский элемент кнопки &quot;Добавить в корзину&quot;
			itemId = this.getAttribute('data-id'), // ID товара
			itemTitle = parentBox.querySelector('.item_title').innerHTML; // название товара
			itemPrice = parentBox.querySelector('#prodPrice').innerHTML;
			itemCol = parentBox.querySelector('#kol').innerHTML;//цена товара
	if(!cartData.hasOwnProperty(itemId)){	// если такой товар уже в корзине, то добавляем +1 к его количеству
		cartData[itemId] = [itemTitle, itemPrice,itemCol];
	}
	// Обновляем данные в LocalStorage
	if(!setCartData(cartData)){ 
		this.disabled = false; // разблокируем кнопку после обновления LS
		cartCont.innerHTML = 'Товар добавлен в корзину.';
		setTimeout(function(){
			cartCont.innerHTML = 'Продолжить покупки...';
		}, 1000);
	}
	return false;
}
// Устанавливаем обработчик события на каждую кнопку &quot;Добавить в корзину&quot;
for(var i = 0; i < itemBox.length; i++){
	addEvent(itemBox[i].querySelector('.add_item'), 'click', addToCart);
}
// Открываем корзину со списком добавленных товаров
function openCart(e){
	
	var cartData = getCartData(), // вытаскиваем все данные корзины
			totalItems = '';
	console.log(JSON.stringify(cartData));
	// если что-то в корзине уже есть, начинаем формировать данные для вывода
	if(cartData !== null){
		totalItems = '<table class="shopping_list"><tr><th>Наименование</th><th>Цена</th><th>Кол-во</th>';
		for(var items in cartData){
			totalItems += '<tr>';
			for(var i = 0; i < cartData[items].length; i++){
				totalItems += '<td>' + cartData[items][i] + '</td>';
			}
			totalItems += '</tr>';
		}
		totalItems += '<table>';
		cartCont.innerHTML = totalItems;
	} else {
		// если в корзине пусто, то сигнализируем об этом
		cartCont.innerHTML = 'В корзине пусто!';
	}
	return false;
}
/*Открыть корзину 
addEvent(d.getElementById('checkout'), 'click', openCart);
/*Очистить корзину
addEvent(d.getElementById('clear_cart'), 'click', function(e){
	localStorage.removeItem('cart');
	cartCont.innerHTML = 'Корзина очишена.';	
});*/;
